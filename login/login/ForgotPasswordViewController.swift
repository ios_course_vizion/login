//
//  ViewController.swift
//  login
//
//  Created by Vu, Dinh Van  on 11/30/20.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var resetButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtUser.layer.borderColor = UIColor(rgb: 0x1A4F8B).cgColor
        txtUser.layer.borderWidth = 1.0
        txtUser.layer.cornerRadius = 5
        resetButton.layer.cornerRadius = 5
        createLeftIcon(textField: txtUser, icon: "contact_icon.png")
        txtUser.attributedPlaceholder = NSAttributedString(string: "example@email.com",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x1A4F8B)])
    }


    func createLeftIcon(textField: UITextField, icon: String) {
        let userImageView = UIImageView()
        userImageView.image = UIImage(named: icon)
        userImageView.contentMode = .scaleAspectFit
        let view = UIView()
        userImageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(userImageView)
        view.widthAnchor.constraint(equalToConstant: 30).isActive = true
        userImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        userImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -3).isActive = true
        textField.leftViewMode = .always
        textField.leftView = view
    }
    
    @IBAction func clickBackLogin(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

